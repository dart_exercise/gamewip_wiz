import 'dart:io';
import 'Game.dart';
import 'Monster.dart';
import 'Store.dart';
import 'TableMap.dart';
import 'Object.dart';
import 'package:chalkdart/chalk.dart';

class Wizard extends Object {
  late TableMap map;
  late Store store = new Store(0, 0);
  late Game game = Game();
  late Monster monster = new Monster(0, 0, 0);
  late int x, y;
  late int HP = 150;
  late int defaultHP = 150;
  late int dmg1 = 5;
  late int dmg2 = 10;
  late String name;
  late int cash = 0;
  late int potions = 0;
  late int count;
  Wizard(String symbol, int x, int y, TableMap map) : super(symbol, x, y) {
    this.symbol = symbol;
    this.x = x;
    this.y = y;
    this.map = map;
  }

  //walk
  bool walk(String direction, map) {
    switch (direction) {
      case 'W':
      case 'w':
        if (walkN()) {
          return false;
        }
        break;
      case 'S':
      case 's':
        if (walkS()) {
          return false;
        }
        break;
      case 'D':
      case 'd':
        if (walkE()) {
          return false;
        }

        break;
      case 'A':
      case 'a':
        if (walkW()) {
          return false;
        }
        break;
      default:
        return false;
    }
    // check();
    return true;
  }

  int getHPWiz() {
    return this.HP;
  }

  void setHPWiz(int HP) {
    this.HP = HP;
  }

  int getHPDefault() {
    return this.defaultHP;
  }

  void setHPDefault(int defaultHP) {
    this.defaultHP = defaultHP;
  }

  int getDmg1() {
    return this.dmg1;
  }

  int getDmg2() {
    return this.dmg2;
  }

  int getPotions() {
    return this.potions;
  }

  int getCash() {
    return this.cash;
  }

  void setPotions(int potions) {
    this.potions = potions;
  }

  bool canWalk(int x, int y) {
    return map.inMap(x, y) && !map.isStar(x, y);
  }

  bool walkW() {
    if (canWalk(x - 1, y)) {
      x = x - 1;
    } else {
      return true;
    }
    return false;
  }

  bool walkE() {
    if (canWalk(x + 1, y)) {
      x = x + 1;
    } else {
      return true;
    }
    return false;
  }

  bool walkS() {
    if (canWalk(x, y + 1)) {
      y = y + 1;
    } else {
      return true;
    }
    return false;
  }

  bool walkN() {
    if (canWalk(x, y - 1)) {
      y = y - 1;
    } else {
      return true;
    }
    return false;
  }

  String nameWizard(name) {
    this.name = name;
    print(chalk.hex('#00FFC9').bold("\nNice to meet you $name!! ❤️​"));
    var key = stdin.readLineSync()!;
    print(chalk.red("""
 ██     ██ ████████ ██      ███████  ██████  ███       ███ ████████     ████████  ██████     
 ██     ██ ██       ██     ██       ██    ██ ████     ████ ██              ██    ██    ██    
 ██  █  ██ ███████  ██     ██       ██    ██ ██ ██   ██ ██ ███████         ██    ██    ██   
 ██ ███ ██ ██       ██     ██       ██    ██ ██  ██ ██  ██ ██              ██    ██    ██     
  ███ ███  ████████ ██████  ███████  ██████  ██   ███   ██ ████████        ██     ██████ 
    """));

    print(chalk.red("""
██     ██ ██████ ████████ ████████  ███████  ██████       ██     ██  ██████   ███████  ██     ██████   ██
██     ██   ██        ██  ██    ██  ██    ██ ██    ██     ██     ██ ██    ██  ██    ██ ██     ██    ██ ██
██  █  ██   ██      ███   ████████  ████████ ██    ██     ██  █  ██ ██    ██  ████████ ██     ██    ██ ██
██ ███ ██   ██    ██      ██    ██  ██  ██   ██    ██     ██ ███ ██ ██    ██  ██  ██   ██     ██    ██
 ███ ███  ██████ ████████ ██    ██  ██  ████ ██████        ███ ███   ██████   ██  ████ ██████ ██████   ██
    """));
    print(chalk.hex('#15E8B8').bold("\nHow to play ?"));
    print(
        chalk.hex('#00A3FE').bold("1. Walk key is 'w' 'a' 's' 'd' and enter"));
    print(chalk.hex('#00A3FE').bold("2. 'W' is your character"));
    print(chalk
        .hex('#00A3FE')
        .bold("3. 'H' is Treasure (you will get cash when founded)"));
    print(chalk
        .hex('#00A3FE')
        .bold("4. 'S' is Store (you can buy item and upgrade your status)"));
    print(chalk
        .hex('#00A3FE')
        .bold("5. 'X' is Monster (fight for receive cash)"));
    print(chalk
        .hex('#00A3FE')
        .bold("6. 'B' is Boss (last monster before clear this game)"));
    print(chalk.hex('#00A3FE').bold("7. '>' is exit door"));
    print(chalk.hex('#FEF200').bold("\nAre you ready ?"));
    print(chalk.bold("\nPress Enter to continue.."));
    key = stdin.readLineSync()!;
    print(chalk.hex('#15E8B8').bold("Let's play 💨​"));
    print(chalk.green.bold("\n[short story]"));
    print(chalk.yellow.bold(
        "...You are a wizard of the village, But then one day the dragon came to attack the village."));
    print(chalk.yellow.bold(
        "So a hero who has the ability to fight with Dragon must travel to kill it! 😈​"));
    print(chalk
        .hex('#FF0000')
        .bold("\nWhich that person is you! $name!! hahaaaa"));
    key = stdin.readLineSync()!;
    print("\n");
    return name;
  }

  String getName() {
    return this.name;
  }

  String showSkills(int count) {
    print(chalk.hex('#00FFEA').bold("Skill :"));
    print(chalk.hex('#B8FF3C').bold("1. Fire ball"));
    print(chalk.hex('#B8FF3C').bold("2. Thunder bolt [total $count]"));
    print(chalk.hex('#B8FF3C').bold("3. back"));
    String input = stdin.readLineSync()!;
    return input;
  }

  String showPotions() {
    print(chalk.bold("You have $potions bottles"));
    print(chalk.bold("Do you want to use potions?"));
    print(chalk.red.bold("1. Yes"));
    print(chalk.blue.bold("2. No"));
    String choose = stdin.readLineSync()!;
    return choose;
  }

  void checkBag() {
    print(chalk.bold("\n$name BAG"));
    print(chalk.bold("Cash $cash"));
    print(chalk.bold("HP $HP"));
    print(chalk.bold("Fire ball $dmg1 dmg."));
    print(chalk.bold("Thunder bolt $dmg2 dmg."));
    print(chalk.bold("Potions have $potions"));
    print(chalk.bold("\nPress Enter to continue.."));
    var key = stdin.readLineSync()!;
  }

  void checkStore() {
    var input = "";
    if (map.isStore(x, y) && input != "5" ||
        (x == 5 && y == 1) ||
        (x == 27 && y == 3) ||
        (x == 5 && y == 4) ||
        (x == 22 && y == 6)) {
      print(chalk.hex('#FAA041').bold("Your cash : $cash"));
      input = store.showStore();
      if (input == "1" && cash >= 50) {
        HP += 100;
        //DefaultHP
        defaultHP += 100;
        cash -= 50;
        checkStore();
      } else if (input == "2" && cash >= 20) {
        dmg1 += 3;
        cash -= 20;
        checkStore();
      } else if (input == "3" && cash >= 25) {
        dmg2 += 10;
        cash -= 25;
        checkStore();
      } else if (input == "4" && cash >= 100) {
        potions += 1;
        cash -= 100;
        checkStore();
      } else if (input == "5") {
        print(chalk.hex('#FFFFFF').bold("Exit Store"));
        var key = stdin.readLineSync()!;
      } else {
        print(chalk.hex('#FFFFFF').bold("You don't have enough cash to buy."));
        var key = stdin.readLineSync()!;
        checkStore();
      }
    }
  }

  void checkTreasure() {
    if (map.isTreasure(x, y) ||
        (x == 4 && y == 1) ||
        (x == 25 && y == 2) ||
        (x == 6 && y == 3) ||
        (x == 19 && y == 3) ||
        (x == 21 && y == 3) ||
        (x == 1 && y == 6)) {
      int treasure = map.reCash(x, y);
      if (treasure > 0) {
        print(chalk.hex('#8CFF00').bold("Receive 200cash"));
        cash += treasure;
        //new set h
        // Treasure treasureh = Treasure("h", x, y, 0);
        // map.setTreasure(treasureh);
        var key = stdin.readLineSync()!;
      } else {
        print(chalk.hex('#FFFFFF').bold("This treasure is empty"));
        var key = stdin.readLineSync()!;
      }
    }
  }

  bool checkMonster(int count) {
    if (map.isMonster(x, y) ||
        (x == 10 && y == 1) ||
        (x == 13 && y == 2) ||
        (x == 26 && y == 2) ||
        (x == 17 && y == 3) ||
        (x == 20 && y == 3) ||
        (x == 7 && y == 4) ||
        (x == 26 && y == 4) ||
        (x == 4 && y == 5) ||
        (x == 11 && y == 5) ||
        (x == 21 && y == 5) ||
        (x == 24 && y == 6)) {
      int hpMonster = map.checkMonsterHP(x, y);
      if (hpMonster > 0) {
        print(chalk.hex('#FFFFFF').bold("\nFound MONSTER!!"));
        print(chalk.hex('#97FF00').bold("\n[...Entered to Battle...]"));
        bool status = map.attackMonster(x, y, count, hpMonster);
        if (status != true) {
          return false;
        } else {
          return true;
        }
      } else if (map.monsterl(x, y) == "l") {
        print(chalk.hex('#45FF00').bold("This monster already died"));
        var key = stdin.readLineSync()!;
      }
    }
    return false;
  }

  bool checkBoss(int count) {
    if (map.isBoss(x, y)) {
      int hpBoss = map.setBoss_d(x, y);
      if (hpBoss > 0) {
        print(chalk.hex('#F85EFF').bold("D..Dra...Dragon!"));
        print(chalk.hex('#97FF00').bold("\nInto the Last Round"));
        print(chalk.hex('#97FF00').bold("\n[...Entered to Boss Fight...]"));
        bool status = map.attackBoss(x, y, count, hpBoss);
        if (status != true) {
          return false;
        } else {
          return true;
        }
      } else if (map.boss_d(x, y) == "d") {
        print(chalk.hex('#45FF00').bold("Boss already died"));
        var key = stdin.readLineSync()!;
      }
    }
    return false;
  }

  bool checkFinish(bool win) {
    if (map.isFinish(x, y)) {
      print(chalk.yellow(""" 

      
██    ██  ██████  ██    ██ 
 ██  ██  ██    ██ ██    ██ 
  ████   ██    ██ ██    ██ 
   ██    ██    ██ ██    ██ 
   ██     ██████   ██████ 


██     ██  ██████  ███    ██  ██  
██     ██ ██    ██ ████   ██  ██ 
██  █  ██ ██    ██ ██ ██  ██  ██  
██ ███ ██ ██    ██ ██  ██ ██  
 ███ ███   ██████  ██   ████  ██ 


      """));

      var key = stdin.readLineSync()!;
      return true;
    } else {
      return false;
    }
  }
}
